import Saludo from "./components/Saludo";
import NuevoComponente from "./components/nuevoComponente";

function App() {
  return (
    <div >
      {/*Nuevo Componente*/}
      <NuevoComponente/>
      {/*Componente con props*/}
      <Saludo nombre="Maximiliano" direccion="Av. Los rosales 123"/>
    </div>
  );
}

export default App;
