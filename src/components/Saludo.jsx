const Saludo = (props) => {
    return(
        <>
            <h2>Saludos {props.nombre}!, direccion: {props.direccion}</h2>
        </>
    )
}

export default Saludo;